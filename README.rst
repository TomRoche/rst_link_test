.. contents:: **Table of Contents**

summary
====

This project has no real content: it's just about exploring how online repositories support internal links.

definitions
====

The following are *terms of art* in the context of this project:

1. ``absolute style``: in filesystems, Unix-style `absolute paths <https://en.wikipedia.org/wiki/Path_%28computing%29#Absolute_and_relative_paths>`_ begin with a ``/`` indicating the root directory. The absolute path to a web resource (such as an online repository) is a `URI <https://en.wikipedia.org/wiki/Uniform_resource_identifier>`_. These can be cumbersome (hard to consume, not portable) for web-content authors.
#. ``relative style``: Unix-style `relative paths <https://en.wikipedia.org/wiki/Path_%28computing%29#Absolute_and_relative_paths>`_ (for both filesystem and web resources) can begin with

    1. ``.`` indicating that the path begins at the current working directory (*CWD*).
    2. ``..`` indicating that the path begins at the parent of the CWD.
    3. a filename or directory name, which is interpreted as if ``../`` was prepended to the path.

#. ``Bitbucket style``: `Bitbucket <https://bitbucket.org/>`_ currently seems to support only the third relative-path style (above), and only such that the name of the project is the first segment of the path. Furthermore this has only been observed to work from Bitbucket project ``README``\s viewed from page=Overview, but may work in other contexts.
#. ``project-absolute style``: for a project-absolute-style path, the leading ``/`` indicates the root directory *of the current project*, thus providing the utility of a traditional absolute path without most of the disadvantages incurred in a web context.
#. ``wiki-absolute style``: Bitbucket (et al?) implement projects and their wikis via separate repositories. Since a wiki has a different repository than its (conceptually) containing project, we can consider the leading ``/`` in a *wiki*\-absolute-style path as indicating the root directory *of the wiki*, not its project.

details
====

arbitrary file in project repo
----

We want to be able to link to other files in the "main" project repo, i.e., objects which are **not** in its

* downloads
* issues
* wiki

project-absolute style
~~~~

1. a file in the project's top-level folder: `</file_in_project_root.txt>`_
#. a file in a child of the project's top-level folder: `</subdir_level_1/file_in_folder_just_below_project_root.rst>`_

relative style
~~~~

1. a file in the project's top-level folder:
    1. path starting with a dot: `<./file_in_project_root.txt>`_
    #. path=filename: `<file_in_project_root.txt>`_
#. a file in a child of the project's top-level folder:
    1. path starting with a dot: `<./subdir_level_1/file_in_folder_just_below_project_root.rst>`_
    #. path starting with first folder name: `<subdir_level_1/file_in_folder_just_below_project_root.rst>`_

Bitbucket style
~~~~

1. a file in the project's top-level folder: `<rst_link_test/file_in_project_root.txt>`_
#. a file in a child of the project's top-level folder: `<rst_link_test/subdir_level_1/file_in_folder_just_below_project_root.rst>`_

downloads
----

We've got downloads! How to reference them?

project-absolute style
~~~~

1. page listing current downloads: `</downloads>`_
#. a specific download: `</downloads/downloadable_1.txt>`_

relative style
~~~~

These should probably fail, since downloads are project resources (not in the filetree):

1. page listing current downloads: `<downloads>`_
#. a specific download: `<downloads/downloadable_1.txt>`_

Bitbucket style
~~~~

1. page listing current downloads: `<rst_link_test/downloads>`_
#. a specific download: `<rst_link_test/downloads/downloadable_1.txt>`_

issues
----

We've got issues! How to reference them?

project-absolute style
~~~~

1. page listing current issues: `</issues>`_
#. page for creating new issue: `</issues/new>`_
#. page for specific issue#=1: `</issue/1>`_

relative style
~~~~

These should probably fail, since issues are project resources (not in the filetree):

1. page listing current issues: `<issues>`_
#. page for creating new issues: `<issues/new>`_
#. page for issue#=1: `<issue/1>`_

Bitbucket style
~~~~

1. page listing current issues: `<rst_link_test/issues>`_
#. page for creating new issues: `<rst_link_test/issues/new>`_
#. page for issue#=1: `<rst_link_test/issue/1>`_

wiki pages
----

We've got a wiki! How to reference its pages?

project-absolute style
~~~~

1. top-level wikipage:

    1. filename with file extension: `</wiki/Home.rst>`_
    #. without file extension: `</wiki/Home>`_

#. another wikipage in its top-level folder:

    1. filename with file extension: `</wiki/another_wikipage_in_top-level_folder.rst>`_
    #. without file extension: `</wiki/another_wikipage_in_top-level_folder>`_

#. wikipage in a child folder/subdir:

    1. filename with file extension: `</wiki/subdir_level_2/wikipage_in_child_folder.rst>`_
    #. without file extension: `</wiki/subdir_level_2/wikipage_in_child_folder>`_

relative style
~~~~

1. top-level wikipage:

    1. with file extension: `<wiki/Home.rst>`_
    #. filename without file extension: `<wiki/Home>`_

#. another wikipage in its top-level folder:

    1. filename with file extension: `</wiki/another_wikipage_in_top-level_folder.rst>`_
    #. without file extension: `</wiki/another_wikipage_in_top-level_folder>`_

#. a wikipage in a child folder/subdir:

    1. filename with file extension: `<wiki/subdir_level_2/wikipage_in_child_folder.rst>`_
    #. without file extension: `<wiki/subdir_level_2/wikipage_in_child_folder>`_

Bitbucket style
~~~~

1. top-level wikipage:

    1. with file extension: `<rst_link_test/wiki/Home.rst>`_
    #. filename without file extension: `<rst_link_test/wiki/Home>`_

#. another wikipage in its top-level folder:

    1. filename with file extension: `<rst_link_test/wiki/another_wikipage_in_top-level_folder.rst>`_
    #. without file extension: `<rst_link_test/wiki/another_wikipage_in_top-level_folder>`_

#. a wikipage in a child folder/subdir:

    1. filename with file extension: `<rst_link_test/wiki/subdir_level_2/wikipage_in_child_folder.rst>`_
    #. without file extension: `<rst_link_test/wiki/subdir_level_2/wikipage_in_child_folder>`_

an image
----

in downloads
~~~~

project-absolute style
!!!!

* ``/downloads/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: /downloads/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   `Jane Austen <https://en.wikipedia.org/wiki/Jane_Austen>`_\'s "`Pride and Prejudice <https://en.wikipedia.org/wiki/Pride_and_Prejudice>`_" summarized by `Jen Sorensen <https://en.wikipedia.org/wiki/Jen_Sorensen>`_

relative style
!!!!

* ``./downloads/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: ./downloads/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   `Jane Austen <https://en.wikipedia.org/wiki/Jane_Austen>`_\'s "`Pride and Prejudice <https://en.wikipedia.org/wiki/Pride_and_Prejudice>`_" summarized by `Jen Sorensen <https://en.wikipedia.org/wiki/Jen_Sorensen>`_

* ``downloads/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: ./downloads/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   `Jane Austen <https://en.wikipedia.org/wiki/Jane_Austen>`_\'s "`Pride and Prejudice <https://en.wikipedia.org/wiki/Pride_and_Prejudice>`_" summarized by `Jen Sorensen <https://en.wikipedia.org/wiki/Jen_Sorensen>`_

Bitbucket style
!!!!

* ``rst_link_test/downloads/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: rst_link_test/downloads/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   `Jane Austen <https://en.wikipedia.org/wiki/Jane_Austen>`_\'s "`Pride and Prejudice <https://en.wikipedia.org/wiki/Pride_and_Prejudice>`_" summarized by `Jen Sorensen <https://en.wikipedia.org/wiki/Jen_Sorensen>`_

in project repo
~~~~

project-absolute style
!!!!

* ``/images/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: /images/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   `Jane Austen <https://en.wikipedia.org/wiki/Jane_Austen>`_\'s "`Pride and Prejudice <https://en.wikipedia.org/wiki/Pride_and_Prejudice>`_" summarized by `Jen Sorensen <https://en.wikipedia.org/wiki/Jen_Sorensen>`_

relative style
!!!!

* ``./images/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: ./images/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   `Jane Austen <https://en.wikipedia.org/wiki/Jane_Austen>`_\'s "`Pride and Prejudice <https://en.wikipedia.org/wiki/Pride_and_Prejudice>`_" summarized by `Jen Sorensen <https://en.wikipedia.org/wiki/Jen_Sorensen>`_

* ``images/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: ./images/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   `Jane Austen <https://en.wikipedia.org/wiki/Jane_Austen>`_\'s "`Pride and Prejudice <https://en.wikipedia.org/wiki/Pride_and_Prejudice>`_" summarized by `Jen Sorensen <https://en.wikipedia.org/wiki/Jen_Sorensen>`_

Bitbucket style
!!!!

* ``rst_link_test/images/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: rst_link_test/images/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   `Jane Austen <https://en.wikipedia.org/wiki/Jane_Austen>`_\'s "`Pride and Prejudice <https://en.wikipedia.org/wiki/Pride_and_Prejudice>`_" summarized by `Jen Sorensen <https://en.wikipedia.org/wiki/Jen_Sorensen>`_
