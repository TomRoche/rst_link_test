.. contents:: **Table of Contents**

summary
====

This file just happens to be in a folder/subdir one level down from the root folder/subdir of our project. It exists solely to see how/if we can link to/from it to other project resources. For terms used below, see section= ``definitions`` in this project's ``README``.

README
====

project-absolute style
----

1. with full file name (including extension): `</README.rst>`_
#. without extension: `</README>`_

relative style
----

1. with full file name (including extension): `<../README.rst>`_
#. without extension: `<../README>`_

Bitbucket style
----

1. with full file name (including extension): `<rst_link_test/README.rst>`_
#. without extension: `<rst_link_test/README>`_

arbitrary file in project repo
====

We want to be able to link to other files in the "main" project repo, i.e., objects which are **not** in its

* downloads
* issues
* wiki

project-absolute style
----

1. another file in the project's top-level folder: `</file_in_project_root.txt>`_

relative style
----

1. another file in the project's top-level folder: `<../file_in_project_root.txt>`_

Bitbucket style
----

1. another file in the project's top-level folder: `<rst_link_test/file_in_project_root.txt>`_

downloads
====

We've got downloads! How to reference them?

project-absolute style
----

1. page listing current downloads: `</downloads>`_
#. a specific download: `</downloads/downloadable_1.txt>`_

relative style
----

These should probably fail, since they are project resources (not in the filetree):

1. page listing current downloads: `<downloads>`_
#. a specific download: `<downloads/downloadable_1.txt>`_

Bitbucket style
----

1. page listing current downloads: `<rst_link_test/downloads>`_
#. a specific download: `<rst_link_test/downloads/downloadable_1.txt>`_

issues
====

We've got issues! How to reference them?

project-absolute style
----

1. page listing current issues: `</issues>`_
#. page for creating new issue: `</issues/new>`_
#. page for specific issue#=1: `</issue/1>`_

relative style
----

These should probably fail, since they are project resources (not in the filetree):

1. page listing current issues: `<issues>`_
#. page for creating new issues: `<issues/new>`_
#. page for issue#=1: `<issue/1>`_

Bitbucket style
----

1. page listing current issues: `<rst_link_test/issues>`_
#. page for creating new issue: `<rst_link_test/issues/new>`_
#. page for specific issue#=1: `<rst_link_test/issue/1>`_

wiki pages
====

We've got a wiki! How to reference its pages?

project-absolute style
----

1. top-level wikipage:

    1. filename with file extension: `</wiki/Home.rst>`_
    #. without file extension: `</wiki/Home>`_

#. another wikipage in its top-level folder:

    1. filename with file extension: `</wiki/another_wikipage_in_top-level_folder.rst>`_
    #. without file extension: `</wiki/another_wikipage_in_top-level_folder>`_

#. wikipage in a child folder/subdir:

    1. filename with file extension: `</wiki/subdir_level_2/wikipage_in_child_folder.rst>`_
    #. without file extension: `</wiki/subdir_level_2/wikipage_in_child_folder>`_

relative style
----

These should probably work, since they *are* in the filetree (though in a separate repo--in Bitbucket, anyway):

1. top-level wikipage:

    1. with file extension: `<../wiki/Home.rst>`_
    #. filename without file extension: `<../wiki/Home>`_

#. another wikipage in its top-level folder:

    1. filename with file extension: `<../wiki/another_wikipage_in_top-level_folder.rst>`_
    #. without file extension: `<../wiki/another_wikipage_in_top-level_folder>`_

#. a wikipage in a child folder/subdir:

    1. filename with file extension: `<../wiki/subdir_level_2/wikipage_in_child_folder.rst>`_
    #. without file extension: `<../wiki/subdir_level_2/wikipage_in_child_folder>`_

Bitbucket style
----

1. top-level wikipage:

    1. with file extension: `<rst_link_test/wiki/Home.rst>`_
    #. filename without file extension: `<rst_link_test/wiki/Home>`_

#. another wikipage in its top-level folder:

    1. filename with file extension: `<rst_link_test/wiki/another_wikipage_in_top-level_folder.rst>`_
    #. without file extension: `<rst_link_test/wiki/another_wikipage_in_top-level_folder>`_

#. a wikipage in a child folder/subdir:

    1. filename with file extension: `<rst_link_test/wiki/subdir_level_2/wikipage_in_child_folder.rst>`_
    #. without file extension: `<rst_link_test/wiki/subdir_level_2/wikipage_in_child_folder>`_

an image
====

in downloads
----

project-absolute style
~~~~

* ``/downloads/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: /downloads/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   `Jane Austen <https://en.wikipedia.org/wiki/Jane_Austen>`_\'s "`Pride and Prejudice <https://en.wikipedia.org/wiki/Pride_and_Prejudice>`_" summarized by `Jen Sorensen <https://en.wikipedia.org/wiki/Jen_Sorensen>`_

relative style
~~~~

* ``../downloads/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: ../downloads/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   `Jane Austen <https://en.wikipedia.org/wiki/Jane_Austen>`_\'s "`Pride and Prejudice <https://en.wikipedia.org/wiki/Pride_and_Prejudice>`_" summarized by `Jen Sorensen <https://en.wikipedia.org/wiki/Jen_Sorensen>`_

Bitbucket style
~~~~

* ``rst_link_test/downloads/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: rst_link_test/downloads/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   `Jane Austen <https://en.wikipedia.org/wiki/Jane_Austen>`_\'s "`Pride and Prejudice <https://en.wikipedia.org/wiki/Pride_and_Prejudice>`_" summarized by `Jen Sorensen <https://en.wikipedia.org/wiki/Jen_Sorensen>`_

in project repo
----

project-absolute style
~~~~

* ``/images/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: /images/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   `Jane Austen <https://en.wikipedia.org/wiki/Jane_Austen>`_\'s "`Pride and Prejudice <https://en.wikipedia.org/wiki/Pride_and_Prejudice>`_" summarized by `Jen Sorensen <https://en.wikipedia.org/wiki/Jen_Sorensen>`_

relative style
~~~~

* ``../images/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: ../images/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   `Jane Austen <https://en.wikipedia.org/wiki/Jane_Austen>`_\'s "`Pride and Prejudice <https://en.wikipedia.org/wiki/Pride_and_Prejudice>`_" summarized by `Jen Sorensen <https://en.wikipedia.org/wiki/Jen_Sorensen>`_

Bitbucket style
~~~~

* ``rst_link_test/images/Jen_Sorensen_PandP-poster-ForWeb.png``:

.. figure:: rst_link_test/images/Jen_Sorensen_PandP-poster-ForWeb.png
   :scale: 100 %
   :alt: alternate text
   :align: center

   `Jane Austen <https://en.wikipedia.org/wiki/Jane_Austen>`_\'s "`Pride and Prejudice <https://en.wikipedia.org/wiki/Pride_and_Prejudice>`_" summarized by `Jen Sorensen <https://en.wikipedia.org/wiki/Jen_Sorensen>`_
